<!DOCTYPE html> 
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Submit Form</title>
<body>
<center>
<fieldset style='width: 550px; height: 550px; border:#5db45d solid'>
    <?php
        session_start();
    ?>
    <form style='margin: 50px 70px 0 50px' method="post">
    <table style = 'border-collapse: separate; border-spacing: 15px 15px;'>
            <tr height = '40px'>
            <td style = 'background-color: #5db45d; 
                 text-align: left; padding: 5px 5px'>
                    <label style='color: white; width: 0%;'>Họ và tên</label>
                </td>
                <td >
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["name"]."</div>"
                    ?>
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 40% style = 'background-color: #5db45d; 
                 text-align: left; padding: 5px 15px'>
                    <label style='color: white;'>Giới tính</label>
                </td>
                <td >
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["gender"]."</div>"
                    ?>  
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #5db45d; 
                text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Phân Khoa</label>
                </td>
                <td height = '40px'>
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["group"]."</div>"
                    ?>  
                </td>
            </tr>
            
            <tr height = '40px'>
                <td style = 'background-color:#5db45d; 
                vertical-align: top; text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Ngày sinh</label>
                </td>
                <td height = '40px'>
                    <?php
        
                        $dateOfBirth = $_SESSION["dateOfBirth"];
                        echo "<div style='color: black;'>".date("d-m-Y", strtotime($dateOfBirth))."</div>"
                    ?>  
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #5db45d; 
                 text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <?php
                        echo "<div style='color: black;'>".$_SESSION["address"]."</div>"
                    ?>  
                </td>
            </tr>

            <tr height = '100px'>
                <td style = 'background-color: #5db45d; 
                text-align: left; padding: 15px 15px'>
                    <label style='color: white;'>Hình ảnh </label>
                </td>
                <td height = '100px'>
                    <?php
                        echo "<img src='".$_SESSION["file"]."' width='100' height='100'>";
                    ?>  
                </td>
            </tr>

        </table>
        <input type='submit' value='Xác nhận' id='submit' 
        style='background-color: #5db45d; 
        border-radius: 10px; 
        width: 35%; height: 43px; 
        border: 1.5px solid #2e8bc0;
        margin: 20px 130px; 
        color: white'/>
    </form>

</fieldset>
</center>
</body>
</html>


