<!DOCTYPE html> 
<html lang='vn'> 
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head> 
<style>
    .error{
        color: red;
    }
</style>
<title>Sign Up Form</title>
<body>
    <fieldset style='width: 500px; height: 550px; border:#ADD8E6 solid; margin:0 auto'>
    <?php
    session_start();
        $name = $gender = $group = $dateOfBirth = $address= "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $check = true;
            if(empty(inputHandling($_POST["name"]))){
                echo "<div class='error'>Hãy nhập tên</div>";
                $check= false;

            }
            if (empty($_POST["gender"])) {

                echo "<div class='error'>Hãy chọn giới tính</div>";
                $check= false;

            }
            if(empty(inputHandling($_POST["group"]))){

                echo "<div class='error'>Hãy chọn phân khoa</div>";
                $check= false;

            }

            if(empty(inputHandling($_POST["dateOfBirth"]))){

                echo "<div class='error'>Hãy nhập ngày sinh</div>";
                $check= false;

            }
            elseif (!validationDate($_POST["dateOfBirth"])) {

                echo "<div class='error'>Hãy nhập ngày sinh đúng định dạng</div>";
                $check= false;

            }
            if($check){
                $_SESSION = $_POST;
                header("Location:submit.php");
                exit();


            }
           
    
        }

        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validationDate($date){
            if (preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/',$date)) {
                return true;
            } else {
                return false;
            }

        }
        
    ?>
    <form style='margin: 20px 50px 0 35px' method="post">
        <table style = 'border-collapse:separate; border-spacing:25px 15px;'>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #5db45d; 
                text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white; font-size: 18px;'>Họ và tên</label>
                    <span class="error" >*</span>
                </td>
                <td width = 30%   >
                    <input type='text' name = "name" style = 'line-height: 32px; 
                    border: 1.5px solid #2d6d98; 
                    width: 100%;'>

                    
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #5db45d; 
                text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white; font-size: 18px;'>Giới tính</label>
                    <span class="error" >*</span>

                </td>
                <td width = 30% >
                <?php
                    $genderArr=array("Nam","Nữ");
                    for($x = 0; $x < count($genderArr); $x++){
                        echo"
                            <label class='container'>
                                <input type='radio' value=".$genderArr[$x]." name='gender'>"
                                .$genderArr[$x]. 
                            "</label>";
                    }
                ?>  

                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #5db45d; 
                text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white;font-size: 18px;'>Phân Khoa</label>
                    <span class="error">*</span>

                </td>
                <td height = '40px'>
                    <select name='group' style = 'border-color:#5b9bd5;height: 35px;width: 80%;'>
                        <?php
                            $facultyArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($facultyArr as $x=>$x_value){
                                echo"<option>".$x_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr height = '40px'>
                <td style = 'background-color: #5db45d; 
                text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white;font-size: 18px;'>Ngày sinh</label>
                    <span class="error" >*</span>

                </td>
                <td height = '40px'>
                    <input type='date' name="dateOfBirth" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border: 1.5px solid #2d6d98; color: grey'>
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #5db45d; 
                text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white; font-size: 18px;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <input type='text' name="address" style = 'line-height: 32px; 
                    border: 1.5px solid #2d6d98; 
                    width: 100%;'> 

                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #5db45d; 
                text-align: center; padding: 5px 5px;
                border: 1.5px solid #2E8BC0'>
                    <label style='color: white; font-size: 18px;'>Hình ảnh</label>
                    
                    <td width = 30%>
                    <form action="/upload.php" method="post" enctype="multipart/form-data">
                        <input type="file" name="file" id="fileupload">
                    </form>
                </td>
                    
                </td>
                
            </tr>

        </table>
        
        <input type='submit' value='Đăng ký' id='submit' 
        style='background-color: #5db45d; 
        border-radius: 10px; 
        width: 35%; height: 43px; 
        border: 1.5px solid #2e8bc0;
        margin: 20px 130px; 
        color: white'/>
    </form>
</fieldset>
</body>
</html>


